import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './pages/admin/admin.component';
import { AsesorComponent } from './pages/asesor/asesor.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [
  {
    path:'', 
    
    children:[
      {path: '',
      component:LoginComponent,
      },
      {path: 'Asesoria',
        component:AsesorComponent,
        loadChildren: () => import('./pages/asesor/asesor.module').then(m => m.AsesorModule)
      },
      {path: 'Administracion',
      component: AdminComponent,
      loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule)
    }
    ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
