import { Component, OnInit } from '@angular/core';
import {  NgForm } from '@angular/forms';
import { Router } from '@angular/router';

interface usuarios{
  usuario:string,
  contraseña:string,
  roll:string
}

const usuario:usuarios={
  usuario:"Admin",
  contraseña:"Admin",
  roll:"Admin"
}
;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  logo:string;
  Usuario =usuario;

  constructor(public router: Router) { }

  ngOnInit(): void {
    this.logo='https://st.depositphotos.com/1251465/4343/v/600/depositphotos_43436705-stock-illustration-icon-design-element.jpg'
  }

  ingresar(datos:NgForm){
    console.log(datos.value);

    if(datos.value.usuario===this.Usuario.usuario && datos.value.Contraseña ===this.Usuario.contraseña){
        if(datos.value.Rol === "admin"){
          this.router.navigate(["Administracion"])
          console.log("ingreso");
          
        }else{
          this.router.navigate(["Asesoria"])
        }
      }
    
  }

}
