import { Routes } from '@angular/router';
import { AgendamientoComponent } from './components/agendamiento/agendamiento.component';
import { AsociadosComponent } from './components/asociados/asociados.component';


export const AdminRoutes: Routes = [
    {
        path:"",
        children:[
            {
                path:"asociados",
                component: AgendamientoComponent
            }, {
                path:"",
                component: AsociadosComponent
            },

        ]
        
    },
    {
        
    }
]