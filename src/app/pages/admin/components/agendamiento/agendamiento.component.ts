import { Component, OnInit, ViewChild} from '@angular/core';

import { Router } from '@angular/router';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';

interface pais {
id: string;
name: number;
flag: string;
area: string;
population: string;
}

const COUNTRIES: pais[] = [
  {
  id: "Rosa alvira",
  name: 3123456798,
  flag: '24/12/2021',
  area: "Todo riesgo",
  population:'carlos celis'
  },
  {
  id: "Damien quiroga",
  name: 3123456798,
  flag: '23/12/2021',
  area: "Automovil",
  population: 'carlos celis'
  },
  {
  id: "Alexis Maldonado",
  name: 3123456798,
  flag: '02/12/2021',
  area: "Motosicleta",
  population: 'carlos celis'
  },
  {
  id:"Andres Fernandez",
  name: 3123456798,
  flag: '15/11/2021',
  area: "Vivienda",
  population: 'carlos celis'
  },
  {
  id:"Andres Fernandez",
  name: 3123456798,
  flag: '15/11/2021',
  area: "Vivienda",
  population: 'carlos celis'
  },
  {
  id:"Andres Fernandez",
  name: 3123456798,
  flag: '15/11/2021',
  area: "Vivienda",
  population: 'carlos celis'
  },
  {
  id:"Andres Fernandez",
  name: 3123456798,
  flag: '15/11/2021',
  area: "Vivienda",
  population: 'carlos celis'
  }
];

/* export type SortColumn = keyof pais | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: {[key: string]: SortDirection} = { 'asc': 'desc', 'desc': '', '': 'asc' };

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
column: SortColumn;
direction: SortDirection;
}

@Directive({
selector: 'th[sortable]',
host: {
'[class.asc]': 'direction === "asc"',
'[class.desc]': 'direction === "desc"',
'(click)': 'rotate()'
}
})
export class NgbdSortableHeader {

@Input() sortable: SortColumn = '';
@Input() direction: SortDirection = '';
@Output() sort = new EventEmitter<SortEvent>();

rotate() {
this.direction = rotate[this.direction];
this.sort.emit({column: this.sortable, direction: this.direction});
}
}
 */

@Component({
  selector: 'app-agendamiento',
  templateUrl: './agendamiento.component.html',
  styles: [
  ]
})
export class AgendamientoComponent implements OnInit {
  
  countries = COUNTRIES;
  temp:any = [];
  busc:pais[];
  rows:pais[];
  
  /* @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>; */
  @ViewChild(DatatableComponent) table: DatatableComponent;

  ColumnMode = ColumnMode;
  constructor ( public router: Router ) {
    this.busc = [...COUNTRIES];
    this.rows= [...COUNTRIES];
    console.log(this.rows);
  }
  
  ngOnInit(): void {
  }

/* 
  onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting countries
    if (direction === '' || column === '') {
      this.countries = COUNTRIES;
    } else {
      this.countries = [...COUNTRIES].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  }
 */
}
