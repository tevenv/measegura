import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminRoutes } from './admin.routing';
import { AsociadosComponent } from './components/asociados/asociados.component';
import { AgendamientoComponent } from './components/agendamiento/agendamiento.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';



@NgModule({
  declarations: [
    AsociadosComponent,
    AgendamientoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AdminRoutes),
    NgbNavModule,
    NgxDatatableModule
  ]
})
export class AdminModule { }
