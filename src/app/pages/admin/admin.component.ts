import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { empty } from 'rxjs';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  salir:boolean =false;
  active = 1;
  pagina = 1;
  nombre = "AdminJohana"

  constructor(public router:Router, config: NgbModalConfig, private modalService: NgbModal){ 
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
  }

  navegar(ruta:string){
      if(ruta==='Solicitar'){
        this.router.navigate(["Administracion"])
      }else{
        this.router.navigate(["Administracion/asociados"])
      }
  }

  mostrar(res:string){
    if (this.salir) {
      this.salir=false
    } else {
      this.salir=true
    }
    if(res === 'salir'){
      this.router.navigate([" "])
    }
  }

}
