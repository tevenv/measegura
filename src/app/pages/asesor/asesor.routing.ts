import { Routes } from '@angular/router';
import { AgendamientoComponent } from './components/agendamiento/agendamiento.component';
import { AsociadosComponent } from './components/asociados/asociados.component';
import { DocumentosComponent } from './components/documentos/documentos.component';
import { InfoClienteComponent } from './components/info-cliente/info-cliente.component';
import { RegistroComponent } from './components/registro/registro.component';

export const AsesorRoutes: Routes = [
    {
        path:"",
        children:[
            {
                path:"",
                component: AgendamientoComponent
            },
            {
                path:"informacion",
                component: InfoClienteComponent
            },
            {
                path:"documentos",
                component: DocumentosComponent
            },
            {
                path:"asociados",
                component: AsociadosComponent
            },
            {
                path:"registro",
                component: RegistroComponent
            }
        ]
        
    },
    {
        
    }
]