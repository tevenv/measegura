import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgbNavConfig, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-asesor',
  templateUrl: './asesor.component.html',
  styles: [
  ]
})

export class AsesorComponent implements OnInit {

  salir:boolean =false;
  active = 1;
  pagina = 1;
  nombre = "AdminJohana"

  constructor(public router:Router, config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
    config.keyboard = false;
  }

  ngOnInit(): void {
    
  }

  mostrar(){
    if (this.salir) {
      this.salir=false
    } else {
      this.salir=true
    }
    
  }

  navegar(ira:string){
    if(ira==="Solicitar"){
      this.router.navigate(['Asesoria'])
    }else{
      this.router.navigate(['Asesoria/asociados'])
    }
  }

  open(content:any){
    this.modalService.open(content);
  }

  onClick(p:any){
      
  }
}


