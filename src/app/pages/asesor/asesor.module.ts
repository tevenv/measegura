import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AsesorComponent } from './asesor.component';
import { AgendamientoComponent,  } from './components/agendamiento/agendamiento.component';
import { InfoClienteComponent } from './components/info-cliente/info-cliente.component';
import { NgbModule, NgbNavModule, NgbDropdown } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AsesorRoutes } from './asesor.routing';
import { DocumentosComponent } from './components/documentos/documentos.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { AsociadosComponent} from './components/asociados/asociados.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { RegistroComponent } from './components/registro/registro.component';


@NgModule({
  declarations: [
    AsesorComponent,
    AgendamientoComponent,
    InfoClienteComponent,
    DocumentosComponent,
    AsociadosComponent,
    RegistroComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(AsesorRoutes),
    PdfViewerModule,
    NgbNavModule,
    NgbModule,
    NgxDatatableModule 
  ],
  exports: [
    AgendamientoComponent,
    
    InfoClienteComponent,
    DocumentosComponent,
    AsesorComponent
  ],
  bootstrap: [AsesorComponent]
})
export class AsesorModule { }
