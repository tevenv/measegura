import { Component,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ColumnMode } from '@swimlane/ngx-datatable'; 

interface pais {
  Nombre: string;
  Telefono: number;
  Producto: string;
}

const COUNTRIES:pais[] = [
  {
    Nombre: "Rosa alvira",
    Producto: "Todo riesgo",
    Telefono: 3123456798,
  },
  {
    Nombre: "Damien quiroga",
    Telefono: 3123456798,
    Producto: "Automovil",
  },
  {
    Nombre: "Alexis Maldonado",
    Telefono: 3123456798,
    Producto: "Motosicleta",
  },
  {
    Nombre:"Andres Fernandez",
    Telefono: 3123456798,
    Producto: "Vivienda",
  },
  {
    Nombre: "Damien quiroga",
    Telefono: 3123456798,
    Producto: "Automovil",
  },
  {
    Nombre: "Alexis Maldonado",
    Telefono: 3123456798,
    Producto: "Motosicleta",
  },
  {
    Nombre:"Andres Fernandez",
    Telefono: 3123456798,
    Producto: "Vivienda",
  },
  {
    Nombre: "Damien quiroga",
    Telefono: 3123456798,
    Producto: "Automovil",
  },
  {
    Nombre: "Alexis Maldonado",
    Telefono: 3123456798,
    Producto: "Motosicleta",
  },
  {
    Nombre:"Andres Fernandez",
    Telefono: 3123456798,
    Producto: "Vivienda",
  }
];

/* export type SortColumn = keyof pais | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotate: {[key: string]: SortDirection} = { 'asc': 'desc', 'desc': '', '': 'asc' };

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}

@Directive({
  selector: 'th[sortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class NgbdSortableHeader {

  @Input() sortable: SortColumn = '';
  @Input() direction: SortDirection = '';
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({column: this.sortable, direction: this.direction});
  }
}
 */

@Component({
  selector: 'app-agendamiento',
  templateUrl: './agendamiento.component.html',
  styles: [
  ]
})
export class AgendamientoComponent  {
  
  temp:any = [];
  busc:pais[];
  rows:pais[];

  columns = [{ prop: 'Nombre' }, { prop: 'Telefono' }, { prop: 'Producto' }];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  ColumnMode = ColumnMode;
  
  constructor ( public router: Router ) {
      this.busc = [...COUNTRIES];
      this.rows= [...COUNTRIES];
      console.log(this.rows);
  }
  



/*   onSort({column, direction}: SortEvent) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    // sorting countries
    if (direction === '' || column === '') {
      this.countries = COUNTRIES;
    } else {
      this.countries = [...COUNTRIES].sort((a, b) => {
        const res = compare(a[column], b[column]);
        return direction === 'asc' ? res : -res;
      });
    }
  } */

  Contactar(){
    this.router.navigate(['Asesoria/informacion'])
  }

  onClick(index:number){
      console.log(this.rows[index].Nombre)
  }
}
