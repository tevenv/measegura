import {Component,OnInit,ViewChild} from '@angular/core';
import { Router } from '@angular/router';
import { ColumnMode, DatatableComponent } from '@swimlane/ngx-datatable';


interface socio {
  Nombre: string;
  tipID: string;
  id: number;
  Correo: string;
  Telefono: number;
  Producto: string;
  Fecha: string;
  Estado: string;
}

const COUNTRIES: socio[] = [
  {
    Nombre: "DAyana Velasco",
    tipID: "Cedula",
    id: 2121923462,
    Correo: "dayana@hotmail.es",
    Telefono: 3219876534,
    Producto: "SOAT",
    Fecha: "25/11/2021",
    Estado: "Cotizacion"
  },
  {
    Nombre: "tayana Velasco",
    tipID: "Cedula",
    id: 1121923464,
    Correo: "dayana@hotmail.es",
    Telefono: 3219876534,
    Producto: "SALUD",
    Fecha: "25/11/2021",
    Estado: "Rechazo Documentacion"
  },
  {
    Nombre: "DAyana Velasco",
    tipID: "Cedula",
    id: 1121923464,
    Correo: "dayana@hotmail.es",
    Telefono: 3219876534,
    Producto: "Vida individual",
    Fecha: "25/11/2021",
    Estado: "En emisión"
  },
  {
    Nombre: "DAyana Velasco",
    tipID: "Cedula",
    id: 1121923461,
    Correo: "dayana@hotmail.es",
    Telefono: 3209876534,
    Producto: "Bicicleta",
    Fecha: "25/11/2021",
    Estado: "Rechazado"
  }
];
/* 
export type SortColumn = keyof socio | '';
export type SortDirection = 'asc' | 'desc' | '';
const rotat: { [key: string]: SortDirection } = { 'asc': 'desc', 'desc': '', '': 'asc' };

const compare = (v1: string | number, v2: string | number) => v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEven {
  colum: SortColumn;
  directio: SortDirection;
}

@Directive({
  selector: 'th[sortab]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotat()'
  }
})
export class NgbdSortableHeaderas {

  @Input() sortab: SortColumn = '';
  @Input() direction: SortDirection = '';
  @Output() sor = new EventEmitter<SortEven>();

  rotat() {
    this.direction = rotat[this.direction];
    this.sor.emit({ colum: this.sortab, directio: this.direction });
  }
} */

@Component({
  selector: 'app-asociados',
  templateUrl: './asociados.component.html',
  styles: [
  ]
})
export class AsociadosComponent implements OnInit {

  /* @ViewChildren(NgbdSortableHeaderas) headers: QueryList<NgbdSortableHeaderas>;
  countries = COUNTRIES; */

  
  busc:socio[];
  rows:socio[];

  @ViewChild(DatatableComponent) table: DatatableComponent;
  ColumnMode = ColumnMode;
  constructor(public router: Router) {
    this.busc = [...COUNTRIES];
    this.rows= [...COUNTRIES];
    
    console.log(this.rows);
  }

  ngOnInit(): void {
  }

  /* onSor({ colum, directio }: SortEven) {

    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortab !== colum) {
        header.direction = '';
      }
    });

    // sorting countries
    if (directio === '' || colum === '') {
      this.countries = COUNTRIES;
    } else {
      this.countries = [...COUNTRIES].sort((a, b) => {
        const res = compare(a[colum], b[colum]);
        return directio === 'asc' ? res : -res;
      });
    }
  } */

  Documentacion() {
      this.router.navigate(['Asesoria/documentos'])
  }

}
