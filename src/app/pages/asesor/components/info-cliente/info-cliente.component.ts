import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

interface cliente {
  Seguro:string,
  firstname:string,
  lastname:string
  tipid:string,
  id:number,
  cel:number,
  mail:string,
  comment:string
}

const Cliente:cliente ={
  Seguro:"Hogar",
  firstname:"ana",
  lastname:"cordoba",
  tipid:"CC",
  id:1131978453,
  cel:3426780956,
  mail:"algo@gmail.com",
  comment:"rural fuera del perimetro urbano, a las salidas de la carretera principal"
}

@Component({
  selector: 'app-info-cliente',
  templateUrl: './info-cliente.component.html',
  styles: [
  ]
})
export class InfoClienteComponent implements OnInit {

  info = Cliente;
  constructor( public route:Router) { }

  ngOnInit(): void {
    if(this.info.tipid = "CC"){
      this.info.tipid = "Cedula de Ciudadania"
    }
  }

  Continuar(res:string){
    if(res==='acep'){
      this.route.navigate(['Asesoria/'])
      //Enviar documto de contrato
    }else{
      this.route.navigate(['Asesoria/'])
      //Enviar estado de documento rechazado
    }
  }
  

}
