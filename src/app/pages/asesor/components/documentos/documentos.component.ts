import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
/* import { pdfDefaultOptions } from 'ngx-extended-pdf-viewer'; */

interface documento{
    tipo:string,
    aprobado:boolean
}

const Documen:documento[] =[{
  tipo:'cedula',
  aprobado:true
},{
  tipo:'pase',
  aprobado:false
},
{
  tipo:'cedula',
  aprobado:true
},{
  tipo:'pase',
  aprobado:false
},
{
  tipo:'cedula',
  aprobado:true
},{
  tipo:'pase',
  aprobado:false
},
{
  tipo:'cedula',
  aprobado:true
}
]

@Component({
  selector: 'app-documentos',
  templateUrl: './documentos.component.html',
  styles: [ ],
})
export class DocumentosComponent implements OnInit {

  Src = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
  Documento = Documen;

  constructor(private modalService: NgbModal) {
    /* pdfDefaultOptions.assetsFolder = 'bleeding-edge'; */
  }

  ngOnInit(): void {
  
  }

  lanzar(PDF:any){
    this.modalService.open(PDF,{
      size:'xl',
      scrollable:false 
    })
  }

  subirpadf(){
    alert("cargar el PDF");
  }

}
